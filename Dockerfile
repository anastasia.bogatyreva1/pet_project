FROM python:3.8-alpine
RUN apk update \
    && apk add --no-cache gcc \
    musl-dev \
    linux-headers

COPY . /app
WORKDIR /app

RUN pip install --upgrade pip \
    -r requirements.txt

CMD ["python", "main.py"]
