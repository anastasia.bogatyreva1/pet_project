import logging
import config
from aiogram import Bot, Dispatcher, types
from aiogram.types import ParseMode
from aiogram.utils import executor

# Установка уровня логирования
logging.basicConfig(level=logging.INFO)

# Инициализация бота и диспетчера
bot = Bot(token=config.TOKEN)
dp = Dispatcher(bot)

# Обработчик команды /start
@dp.message_handler(commands=['start'])
async def start_command(message: types.Message):
    keyboard = types.InlineKeyboardMarkup()
    button1 = types.InlineKeyboardButton(text="Обо мне", callback_data='button1')
    button2 = types.InlineKeyboardButton(text="Мой стек", callback_data='button2')
    button3 = types.InlineKeyboardButton(text="Мой проект", callback_data='button3')
    button4 = types.InlineKeyboardButton(text="Контакты", callback_data='button4')
    keyboard.add(button1, button2)
    keyboard.add(button3, button4)
    await message.reply("Выберите одну из кнопок:", reply_markup=keyboard)

# Обработчик нажатия на кнопку
@dp.callback_query_handler(lambda query: query.data in ['button1', 'button2', 'button3', 'button4'])
async def process_callback_button(callback_query: types.CallbackQuery):
    if callback_query.data == 'button1':
        await bot.answer_callback_query(callback_query.id)
        await bot.send_message(callback_query.from_user.id, 'В сентябре 2022 года решила сменить специальность на DevOps-инженер. \n\nРесерч курсов не выдал достойного результата, поэтому стала заниматься самообразованием под руководством ментора. В это входило чтение документации, просмотр обучающих видеороликов, чтение литературы и практическая отработка, а также повышение квалификации в ИТМО. \n\nСейчас получаю высшее техническое образование по программе магистратуры "Прикладная информатика" в дистанционном формате, то есть трудовой деятельности обучение никак не помешает.')
    elif callback_query.data == 'button2':
        await bot.answer_callback_query(callback_query.id)
        await bot.send_message(callback_query.from_user.id, 'Linux на уровне администратора \n\nБазовые знания Kubernetes (знаю основные компоненты их их взаимодействие, процесс запуска подов, отличу ReplicaSet от StatefulSet, в качестве пет-проекта переношу этот бот в кластер на bare metal) \n\nГлубокие знания Docker (могу запустить контейнеры даже без Докера, используя возможности Linux) \n\nAnsible (умею писать роли и плэйбуки и знаю, чем они отличаются; знаю, почему нельзя command и shell)')
    elif callback_query.data == 'button3':
        await bot.answer_callback_query(callback_query.id)
        await bot.send_message(callback_query.from_user.id, 'Данный бот - и есть мой проект. \n\nОн написан на Python с помощью библиотеки aiogram. \n\nДалее я его посметила его в Docker-контейнер, создав для этого образ через Dockerfile. \n\nЖелезо для моего "продакшена" было подготовлено с помощью Ansible. \n\nА чтобы автоматизировать этот процесс, я настроила пайплайн в Gitlab CI, который запускается при пуше в Git/')
    elif callback_query.data == 'button4':
        await bot.answer_callback_query(callback_query.id)
        await bot.send_message(callback_query.from_user.id, 'Связаться со мной можно: \n в телеграм - @TheAnastasi \n по телефону - 89651535434 \n по электронной почте - anastasia.bogatyreva@list.ru \n мой гитлаб - https://gitlab.com/anastasia.bogatyreva1/pet_project \n мой hh - https://hh.ru/resume/c2536b89ff0be4cd7a0039ed1f756836335534')

# Запуск бота
if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
